package model.vo;


/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>
{	
	private String taxi_id ;
	private String trip_id ;
	private String trip_start_timestamp ;
	private String trip_end_timestamp ;
	private String trip_seconds ;
	private double trip_miles ;
	private int pickUpCensusTrack ;
	private int dropOffCensusTrack ;
	private String pickup_community_area ;
	private String dropoff_community_area ;
	private String company ;
	private double pickup_centroid_latitude ;
	private double pickup_centroid_longitude ;
	private double dropOffLatitude ;
	private double dropOffLongitude ;
	private FinancialData finData;
	private RangoFechaHora horario;
	private Taxi taxi;
	private String payment_type;
	private String fare;
	private String tips;
	private String tolls;
	private String extras;
	private double harvesiana;

	public Servicio(String taxi_id ,	String tripId ,	String tripStartTimestamp ,	String tripEndTimestamp ,	String tripSeconds ,	double tripMiles ,	int pickUpCensusTrack ,	int dropOffCensusTrack ,	String pickUpCommunityArea ,	String dropOffCommunityArea ,	String company ,	double pickUpLatitude ,	double pickUpLongitude ,	double dropOffLatitude ,	double dropOffLongitude, FinancialData finData ){
		this.setTaxiId(taxi_id);
		this.trip_id = tripId;
		this.trip_start_timestamp = tripStartTimestamp;
		this.trip_end_timestamp = tripEndTimestamp;
		this.trip_seconds = tripSeconds;
		this.trip_miles = tripMiles;
		this.pickUpCensusTrack = pickUpCensusTrack;
		this.dropOffCensusTrack = dropOffCensusTrack;
		this.pickup_community_area = pickUpCommunityArea;
		this.dropoff_community_area = dropOffCommunityArea;
		this.setCompany(company);
		this.pickup_centroid_latitude = pickUpLatitude;
		this.pickup_centroid_longitude = pickUpLongitude;
		this.dropOffLatitude = dropOffLatitude;
		this.dropOffLongitude = dropOffLongitude;
}

	
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getTaxiId() {
		return taxi_id;
	}

	public void setTaxiId(String taxiId) {
		this.taxi_id = taxiId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}
	public Taxi darTaxi()
	{
		taxi= new Taxi(taxi_id,company);
		return taxi;
	}
	public RangoFechaHora darHorario()
	{
		String[] inicial = trip_start_timestamp.split("T");
		String[] fiinal = trip_end_timestamp.split("T");
		horario = new RangoFechaHora(inicial[0], fiinal[0], inicial[1], fiinal[1]);
		return horario;
	}
	public double darTotal()
	{
		finData= new FinancialData(payment_type, Double.parseDouble(fare), Double.parseDouble(tips), Double.parseDouble(tolls), Double.parseDouble(extras));
		return finData.total();
	}
	public String darZonaInicio()
	{
		return pickup_community_area;
	}
	public String darZonaFinal()
	{
		return dropoff_community_area;
	}
	
	public String getTripSeconds(){
		return trip_seconds;
	}
	public String getTripId()
	{
		return trip_id;
	}
	public Double getTripMiles()
	{
		trip_miles = trip_miles*10;
		trip_miles = Math.round(trip_miles);
		trip_miles = trip_miles/10;
		return trip_miles;
	}


	public String getPickupZone() {
		return pickup_community_area;
	}


	public String getDropOffZone() {
		return dropoff_community_area;
	}


	public double getPickupLatitud() {
		// TODO Auto-generated method stub
		return pickup_centroid_latitude;
	}


	public double getPickupLongitud() {
		// TODO Auto-generated method stub
		return pickup_centroid_longitude;
	}
	public void setHarvesiana(double harvesianas)
	{
		harvesianas = Math.floor(harvesianas*10)/10;
		this.harvesiana = harvesianas;
	}
	public double getHarvesiana()
	{
		return harvesiana;
	}
	
}
