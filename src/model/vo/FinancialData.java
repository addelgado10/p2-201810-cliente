package model.vo;

public class FinancialData implements Comparable<FinancialData>{

	//---------------------------
	//	ATRIBUTOS
	//---------------------------
	private String paymentType ;
	private double fare ;
	private double tips ;
	private double tolls ;
	private double extras ;
	private double tripTotal ;

	//---------------------------
	//	CONSTRUCTOR
	//---------------------------
	
	public FinancialData(String paymentType, Double fare, Double tips, Double tolls, Double extras){
	
		this.paymentType=paymentType;
		this.fare=fare;
		this.tips=tips;
		this.tolls=tolls;
		this.extras=extras;
		this.tripTotal =fare + tips - tolls + extras;
	}

	@Override
	public int compareTo(FinancialData arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	public double total()
	{
		return tripTotal;
	}
}
