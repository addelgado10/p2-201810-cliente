package model.vo;

import model.data_structures.Lista;

public class CompaniaServicios implements Comparable<CompaniaServicios> {
	
	private String nomCompania;
	
	private Lista<Servicio> servicios;

	public String getNomCompania() {
		return nomCompania;
	}

	public void setNomCompania(String nomCompania) {
		this.nomCompania = nomCompania;
	}

	public Lista<Servicio> getServicios() {
		return servicios;
	}

	public void setServicios(Lista<Servicio> servicios) {
		this.servicios = servicios;
	}

	@Override
	public int compareTo(CompaniaServicios o) {
		if(servicios.size()>o.servicios.size())
		{
			return 1;
		}
		else if(servicios.size()<o.servicios.size())
			return -1;
		else
			return 0;
	}
	
	

}
