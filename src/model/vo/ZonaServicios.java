package model.vo;

import model.data_structures.Lista;


/**
 * Clase que modela la list de servicios con una zona especifica
 * @author js.martinezn
 *
 */
public class ZonaServicios implements Comparable<ZonaServicios>{

	private String idZona;
	
	private Lista<Servicio> servicios;
	
	

	public String getIdZona() {
		return idZona;
	}



	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public Lista<Servicio> getServicios() {
		return servicios;
	}



	public void setServicios(Lista<Servicio> linkedListtServicios) {
		this.servicios = linkedListtServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
