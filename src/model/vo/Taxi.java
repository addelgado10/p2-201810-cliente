package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>
{
	private String companyY;
	
	private String taxiId;
	
	
	public Taxi(String id,String compania)
	{
		companyY = compania;
		
		taxiId = id;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxiId;
	}
	
	public void setCompany(String comp){
		companyY=comp;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return companyY;
	}
	
	@Override
	public int compareTo(Taxi o) 
	{
		if(taxiId.equals(o.getTaxiId()))
			return 0;
		else
			return 1;
	}	
}