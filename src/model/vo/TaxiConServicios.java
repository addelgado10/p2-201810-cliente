package model.vo;

public class TaxiConServicios implements Comparable{

	private String[] taxiId;
	private RangoFechaHora[] fecha_hora;

	public TaxiConServicios (String[] taxiId, RangoFechaHora[] fecha_hora){
		this.taxiId=taxiId;
		this.fecha_hora=fecha_hora;
		arreglarRangoFechaHora();
	}

	public String [] getTaxiId(){
		return taxiId;
	}
	
	public RangoFechaHora[] getFecha_hora(){
		return fecha_hora;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void print() {
		for (int i = 0 ; i < taxiId.length; i ++){
			System.out.println("Taxi id: "+taxiId[i]);
		}
	}

	private void arreglarRangoFechaHora(){
		RangoFechaHora temp = null;
		String s = null;
		for (int i = 0 ; i < fecha_hora.length; i ++){
			for(int j = 0; j <= i; j++ ){
				System.out.println("i "+i);
				System.out.println("j "+j);
				System.out.println("j "+fecha_hora[j].getFechaInicial());
				if(fecha_hora[j].getFechaInicial().compareTo(fecha_hora[i].getFechaInicial()) >0){
					temp = fecha_hora[j];
					fecha_hora[j]=fecha_hora[i];
					fecha_hora[i]=temp;

					s=taxiId[j];
					taxiId[j]=taxiId[i];
					taxiId[i]=s;
				}
				if(fecha_hora[j].getFechaInicial().compareTo(fecha_hora[i].getFechaInicial()) == 0 && fecha_hora[j].getHoraInicio().compareTo(fecha_hora[i].getHoraInicio())>0){
					temp = fecha_hora[j];
					fecha_hora[j]=fecha_hora[i];
					fecha_hora[i]=temp;

					s=taxiId[j];
					taxiId[j]=taxiId[i];
					taxiId[i]=s;

				}
			}
		}
	}

}
