package model.vo;

public class TaxiConPuntos extends Taxi{


	private double millas;
	
	private double dinero;
	
	private int contador;
	
	public TaxiConPuntos(String id, String compania, double millas, double dinero) {
		super(id, compania);
		this.millas = millas;
		this.dinero = dinero;
		contador = 1;
	}
	
	public double getPuntos(){
		return (millas+dinero)/((dinero/millas)*contador);
	}
	
	public void setMillas(double millas)
	{
		this.millas += millas;
	}
	public void setDinero(double dinero)
	{
		this.dinero += dinero;
	}
	public void sumarContador()
	{
		this.contador++;
	}
	
	public double getMillas()
	{
		return millas;
	}
	public double getDinero()
	{
		return dinero;
	}
	public int getContador()
	{
		return contador;
	}
}
