package model.vo;

import model.data_structures.Lista;

public class ServiciosValorPagado {
	
	private Lista<Servicio> serviciosAsociados;
	private double valorAcumulado;
	public Lista<Servicio> getServiciosAsociados() {
		return serviciosAsociados;
	}
	public void setServiciosAsociados(Lista<Servicio> serviciosAsociados) {
		this.serviciosAsociados = serviciosAsociados;
	}
	public double getValorAcumulado() {
		return valorAcumulado;
	}
	public void setValorAcumulado(double valorAcumulado) {
		this.valorAcumulado = valorAcumulado;
	}
	
	

}
