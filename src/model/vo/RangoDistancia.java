package model.vo;

import model.data_structures.Lista;

/**
 * VO utilizado en Req B1, la lista de servicios cuya distancia recorrida 
 * pertenece a dicha distancia
 */
public class RangoDistancia implements Comparable<RangoDistancia>
{
	/**
	 * Modela el valor m�ximo del rango
	 */
	private double distancia;
	
	/**
	 * Modela la lista de servicios cuya distancia recorrida es distancia 
	 */
	private Lista<Servicio> serviciosEnRango;

	//M�TODOS
	


	/**
	 * @return the limineInferior
	 */
	public double getDistancia() 
	{
		return distancia;
	}

	/**
	 * @param limineInferior the limineInferior to set
	 */
	public void setDistancia(double distancia) 
	{
		this.distancia = distancia;
	}

	/**
	 * @return the serviciosEnRango
	 */
	public Lista<Servicio> getServiciosEnRango() 
	{
		return serviciosEnRango;
	}

	/**
	 * @param serviciosEnRango the serviciosEnRango to set
	 */
	public void setServiciosEnRango(Lista<Servicio> serviciosEnRango)
	{
		this.serviciosEnRango = serviciosEnRango;
	}

	@Override
	public int compareTo(RangoDistancia o)
	{
		return 0;
	}
}
