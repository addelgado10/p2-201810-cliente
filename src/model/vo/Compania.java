package model.vo;

import model.data_structures.Lista;

public class Compania implements Comparable<Compania> {
	
	private String nombre;
	
	private Lista<Taxi> taxisInscritos;	
	

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Lista<Taxi> getTaxisInscritos() {
		return taxisInscritos;
	}

	public void setTaxisInscritos(Lista<Taxi> companias) {
		this.taxisInscritos =  companias;
	}

	@Override
	public int compareTo(Compania o) {

		if(nombre.equals(o.getNombre()))
			return 0;
		else if(nombre.compareTo(o.getNombre())>0)
			return 1;
		else
			return -1;

	}
	
	
	

}
