package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.naming.BinaryRefAddr;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import API.ITaxiTripsManager;
import model.data_structures.BinaryTree;
import model.data_structures.IList;
import model.data_structures.Lista;
import model.data_structures.SeparateCahainingHash;
import model.vo.Compania;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;
import model.vo.Servicio;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.ZonaServicios;
import sun.print.resources.serviceui;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large";


	private BinaryTree<String, 	Compania> arbolTaxis;
	private BinaryTree<Double, RangoDistancia> arbolDistancia;
	private SeparateCahainingHash<String, BinaryTree<Date,Lista<Servicio>>> tAreasServicios ;
	private SeparateCahainingHash<String, Lista<Servicio>> ta1; 
	private SeparateCahainingHash<Integer, Servicio> tDuracionServicios;
	private Lista<Servicio> listaServicios;
	private SeparateCahainingHash<String, Lista<Servicio>> ta2;

	@Override //1C
	public boolean cargarSistema(String direccionJson)  
	{

		boolean resp = true;
		int contador= 0 ;
		Gson gson = new  Gson();
		Lista<Servicio> linkedListtServicios = new Lista();
		arbolTaxis = new BinaryTree();
		arbolDistancia = new BinaryTree<>();
		Lista<Taxi> companias;
		Servicio[] listt = null;
		Servicio[] auxiliar = null;
		try {
			if(!direccionJson.equals(DIRECCION_LARGE_JSON)){
				listt= gson.fromJson(new FileReader(direccionJson), Servicio[].class);	
			}
			else
			{
				for(int i = 1; i < 8; i++)
				{
					Servicio[] list = gson.fromJson(new FileReader(direccionJson+i+".json"), Servicio[].class);
					contador += list.length;
					if(i == 1){
						listt = list;
					}
					else
					{
						auxiliar = new Servicio[listt.length+list.length];
						for(int j = 0;j<listt.length;j++)
						{
							auxiliar[j] = listt[j];
						}
						for(int j = listt.length; j <(listt.length+list.length);j++)
						{
							auxiliar[j] = list[j-listt.length];
						}
						listt = auxiliar;
					}
				}
			}
				
			listaServicios = new Lista<>();
			for(int i = 0; i < listt.length;i++)
			{
				listaServicios.add(listt[i]);
				companias = new Lista<>();
				if(listt[i].getCompany()==null)
				{
					listt[i].setCompany("Independent Owner");
				}
				if(arbolTaxis.contains(listt[i].getCompany()))
				{
					Compania temp = arbolTaxis.get(listt[i].getCompany());
					companias = temp.getTaxisInscritos();
					companias.add(listt[i].darTaxi());
					temp.setTaxisInscritos(companias);
					arbolTaxis.put(listt[i].getCompany(), temp);
				}
				if(!arbolTaxis.contains(listt[i].getCompany()))
				{
					Compania temp = new Compania();
					temp.setNombre(listt[i].getCompany());
					companias.addFirst(listt[i].darTaxi());
					temp.setTaxisInscritos(companias);
					arbolTaxis.put(listt[i].getCompany(), temp);
				}
			}

			//Tabla punto 1A
			Lista<Servicio>[] serv = new Lista[1];
			int disMax=0;
			for(int i = 0; i < listt.length;i++){
				boolean t = false;
				if(listt[i].getTripSeconds()!=null && Integer.parseInt(listt[i].getTripSeconds())>disMax){
					disMax=Integer.parseInt(listt[i].getTripSeconds());
				}
				
				
				if(listt[i].darZonaInicio()!=null){
					if(serv[0]==null){
						serv[0]=new Lista<Servicio>();
						serv[0].addFirst(listt[i]);
					}
					else{
						for (int j = 0 ; j<serv.length ; j ++){
							if( serv[j].getIndex(0).darZonaInicio().equals(listt[i].darZonaInicio())){
								serv[j].addFirst(listt[i]);
								t=true;
							}
						}
					}
					if(!t){
						Lista<Servicio>[] temp = new Lista[serv.length+1];
						for (int k = 0 ; k<serv.length ; k ++){
							temp[k]=new Lista<Servicio>();
							temp[k]=serv[k];
						}
						temp[serv.length]=new Lista<Servicio>();
						temp[serv.length].addFirst(listt[i]);
						serv = temp;
					}
				}
			}

			ta1=new SeparateCahainingHash<>(serv.length);
			for (int j = 0 ; j<serv.length; j ++){
				ta1.put(serv[j].getIndex(0).darZonaInicio(), serv[j]);
			}

			//Tabla punto 2A

			Lista<Servicio>[] duracion = new Lista[(int)Math.ceil(disMax/60)+1];

			for(int i = 0; i< duracion.length; i++){
				duracion[i]=new Lista<Servicio>();
			}

			for (int i = 0; i < listt.length; i++){
				if(listt[i].getTripSeconds()!=null){
					disMax=Integer.parseInt(listt[i].getTripSeconds());
					if(disMax%60 == 0 && disMax !=0){
						duracion[(int) Math.ceil(disMax/60)-1].addFirst(listt[i]);
					}
					else
						duracion[(int) Math.ceil(disMax/60)].addFirst(listt[i]);
				}
			}

			disMax=0;
			for(int i = 0; i < duracion.length; i++){
				if(duracion[i].size()==0){
					disMax++;
				}
			}

			ta2=new SeparateCahainingHash<>(duracion.length-disMax);

			for(int i = 0; i < duracion.length; i++){
				if(duracion[i].size()>0){
					ta2.put(Integer.toString(i), duracion[i]);
				}
			}
			
			
			//Arbol del punto 1B		
			for(int i = 0; i < listt.length;i++)
			{
				linkedListtServicios = new Lista<>();
				if(arbolDistancia.contains(listt[i].getTripMiles()))
				{
					RangoDistancia temp = arbolDistancia.get(listt[i].getTripMiles());
					linkedListtServicios = temp.getServiciosEnRango();
					linkedListtServicios.addFirst(listt[i]);
					temp.setServiciosEnRango(linkedListtServicios);
					arbolDistancia.put(listt[i].getTripMiles(), temp);
				}
				if(!arbolDistancia.contains(listt[i].getTripMiles()))
				{
					RangoDistancia temp = new RangoDistancia();
					temp.setDistancia(listt[i].getTripMiles());
					linkedListtServicios.addFirst(listt[i]);
					temp.setServiciosEnRango(linkedListtServicios);
					arbolDistancia.put(listt[i].getTripMiles(), temp);
				}
			}
			//Tabla de hash del punto 2B
			tAreasServicios = new SeparateCahainingHash<>(listt.length);
			for(int i = 0;i < listt.length;i++)
			{
				BinaryTree<Date, Lista<Servicio>> aux = new BinaryTree<>();
				Lista<Servicio> lista = new Lista<>();
				if(listt[i].darZonaInicio()!=null && listt[i].darZonaFinal()!=null){
					if(tAreasServicios.get(listt[i].darZonaInicio()+"-"+listt[i].darZonaFinal())==null)
					{
						aux = new BinaryTree<>();
						lista = new Lista<>();
						SimpleDateFormat dia = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.000");
						try{
							Date diainicio = dia.parse(listt[i].darHorario().getFechaInicial()+"-"+listt[i].darHorario().getHoraInicio());
							lista.add(listt[i]);
							aux.put(diainicio, lista);
						}catch(ParseException e){
							e.printStackTrace();
						}
						tAreasServicios.put(listt[i].darZonaInicio()+"-"+listt[i].darZonaFinal(), aux);
					}
					else
					{
						aux=new BinaryTree<>();
						lista = new Lista<>();
						aux = tAreasServicios.get(listt[i].darZonaInicio()+"-"+listt[i].darZonaFinal());
						SimpleDateFormat dia = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.000");
						try {
							Date diainicio = dia.parse(listt[i].darHorario().getFechaInicial()+"-"+listt[i].darHorario().getHoraInicio());
							if(aux.get(diainicio)!=null)
								lista = aux.get(diainicio);
							lista.add(listt[i]);
							aux.put(diainicio, lista);
							tAreasServicios.put(listt[i].darZonaInicio()+"-"+listt[i].darZonaFinal(), aux);

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resp;
	}


	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {

		if(ta1.get(Integer.toString(zonaInicio))==null){
			System.out.println("La zona de inicio no existe");
			return null;
		}
		//La tabla no est� devolviendo las lstas con los servicios (se estan borrando)
		Lista<Servicio> areas = ta1.get(Integer.toString(zonaInicio));

		Lista<TaxiConServicios> resp = new Lista<>();

		int[] cant = new int[1];
		String[] txId=new String [1];
		Lista<RangoFechaHora>[] rango = new Lista[1];

		rango[0]=new Lista<>();
		for (int i = 0; i < areas.size(); i++){
			if(areas.getIndex(i).getCompany().equals(compania) && areas.getIndex(i).darHorario()!=null){
				if(txId[0]==null){
					txId[0]=areas.getIndex(i).getTaxiId();
					rango[0].addFirst(areas.getIndex(i).darHorario());
					cant[0]++;
				}
				else{
					for(int j = 0 ; j<txId.length; j++){
						if(txId[j].equals(areas.getIndex(i).getTaxiId())){
							cant[j]++;
							rango[j].addFirst(areas.getIndex(i).darHorario());
						}
						else{
							int[] tempCant = new int[cant.length+1];
							String[] tempTxId=new String [txId.length+1];
							Lista<RangoFechaHora>[] tempRango= new Lista[rango.length+1];
							for(int k = 0 ; k < txId.length; k++){
								tempCant[k]=cant[k];
								tempTxId[k]=txId[k];
								tempRango[k]= rango[k];
							}
							tempCant[cant.length]++;
							tempTxId[txId.length]=areas.getIndex(i).getTaxiId();
							tempRango[txId.length]=new Lista<>();
							tempRango[txId.length].addFirst(areas.getIndex(i).darHorario());
							cant=tempCant;
							txId=tempTxId;
							rango=tempRango;		
							break;
						}
					}
				}
			}
		}

		if(rango[0]==null){
			System.out.println("No se encontro ninguna compa�ia cuyos taxis inicien servicios en la zona deseada");
			return null;
		}

		
		boolean[] pos =new boolean[cant.length];
		int max =0;

		for(int i = 0 ; i <cant.length; i++){
			if(cant[i] >= max){
				if(cant[i] == max){
					pos[i]=true;
				}
				else{
					for(int j = 0 ; j <cant.length; j++){
						pos[j]=false;
					}
					pos[i]=true;
					max = cant[i];
				}
			}
		}

		//Servicio
		max =0;
		int ll=0;
		for(int i = 0 ; i <txId.length; i++){
			if(pos[i]){
				max = max + cant[i];
				ll=ll+rango[i].size();
			}
		}
		
		RangoFechaHora[] rangofh = new RangoFechaHora[ll];
		String[] name= new String [max];
		int conta =0;
		int cont =0;
		for(int i = 0 ; i <txId.length; i++){
			if(pos[i]==true){
				System.out.println("asas");
				for(int j = 0; j<rango[i].size(); j ++){
					rangofh[cont]=rango[i].getIndex(j);
					cont++;
				}
				name[conta]=txId[i];
				conta++;
			}
		}

		TaxiConServicios k = new TaxiConServicios(name, rangofh);
		resp.addFirst(k);

		for(int i = 0 ; i < k.getTaxiId().length; i++){
			System.out.println("Taxi: "+k.getTaxiId()[i]);
			System.out.println("Fecha inicio: "+k.getFecha_hora()[i].getFechaInicial());
			System.out.println("Hora inicio; "+k.getFecha_hora()[i].getHoraInicio()); 
		}
		return resp;
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {

		int aus=Integer.parseInt(Integer.toString(duracion));
		
		
		if(aus%60==0){
			aus=(int)Math.ceil(aus/60)-1;
		}
		else
			aus=(int)Math.ceil(aus/60);

		Lista<Servicio> serv = ta2.get(Integer.toString(aus));

		return serv;
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		Lista<Servicio> resp = new Lista();
		for(double i = distanciaMinima;i < distanciaMaxima;i=i+0.1)
		{
			if(arbolDistancia.contains(i))
			{
				RangoDistancia nuevo = arbolDistancia.get(i);
				Lista<Servicio> temp = nuevo.getServiciosEnRango();
				for(int j = 0;j < temp.size();j++)
				{
					resp.add(temp.getIndex(j));
				}
			}
		}
		return resp;
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		Lista<Servicio> resp = new Lista<>();
		SimpleDateFormat dia = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.000");
		String zona = String.valueOf(zonaInicio)+"-"+String.valueOf(zonaFinal);
		if(tAreasServicios.get(zona)!=null)
		{
			System.out.println("hola");
			BinaryTree<Date, Lista<Servicio>> aux = new BinaryTree<>();
			aux = tAreasServicios.get(zona);
			Lista<Date> aux2 = new Lista<>();
			aux2 = aux.getLLaves();
			try{
				Date fechaIn =  dia.parse(fechaI+"-"+horaI);
				Date fechaFi =  dia.parse(fechaF+"-"+horaF);
				for(int i = 0; i <aux2.size();i++)
				{
					Date temp = aux2.getIndex(i);
					if(temp.compareTo(fechaFi)<=0 && temp.compareTo(fechaIn)>=0)
					{
						for(int j = 0;j < aux.get(temp).size();j++)
						{
							resp.add(aux.get(temp).getIndex(j));
						}

					}
				}
			}catch(ParseException e){

			}

		}
		return resp;
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		BinaryTree<String, TaxiConPuntos> aux = new BinaryTree<>();
		TaxiConPuntos nuevo;
		TaxiConPuntos[] resp;
		for(int i = 0;i < listaServicios.size();i++)
		{
			Servicio temp = listaServicios.getIndex(i);
			if(temp.getTripMiles()>0 && temp.darTotal()>0)
			{
				if(aux.contains(temp.darTaxi().getTaxiId()))
				{
					nuevo = aux.get(temp.darTaxi().getTaxiId());
					nuevo.setDinero(temp.darTotal());
					nuevo.setMillas(temp.getTripMiles());
					nuevo.sumarContador();
					aux.put(temp.darTaxi().getTaxiId(), nuevo);
				}
				else
				{
					nuevo = new TaxiConPuntos(temp.darTaxi().getTaxiId(), temp.darTaxi().getCompany(), temp.getTripMiles(), temp.darTotal());
					aux.put(temp.darTaxi().getTaxiId(), nuevo);
				}
			}
		}
		resp = new TaxiConPuntos[aux.getLLaves().size()];
		for(int i = 0;i < aux.getLLaves().size();i++)
		{
			resp[i] = aux.get(aux.getLLaves().getIndex(i));
		}
		heap(resp);
		return resp;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C) {
		double longitud = 0;
		double latitud = 0;
		int contador = 0;
		double millas = Math.floor(millasReq2C*10)/10.;
		Lista<Servicio> resp = new Lista<>();
		for(int i = 0;i < listaServicios.size();i++)
		{
			if(listaServicios.getIndex(i).getPickupLongitud() != 0 && listaServicios.getIndex(i).getPickupLatitud() != 0)
			{
				longitud += listaServicios.getIndex(i).getPickupLongitud();
				latitud += listaServicios.getIndex(i).getPickupLatitud();
				contador++;
			}
		}
		latitud /= contador;
		longitud /= contador;
		for(int i = 0;i < listaServicios.size();i++)
		{
			if(listaServicios.getIndex(i).getPickupLongitud() != 0 && listaServicios.getIndex(i).getPickupLatitud() != 0)
			{
				Servicio temp = listaServicios.getIndex(i);
				temp.setHarvesiana(getDistance(latitud, longitud, temp.getPickupLatitud(), temp.getPickupLongitud())* 0.00062137);
			}
		}

		SeparateCahainingHash<Double, BinaryTree<String, Lista<Servicio>>> tabla = new SeparateCahainingHash<>(contador);
		for(int i = 0;i < listaServicios.size();i++)
		{
			BinaryTree<String, Lista<Servicio>> aux = new BinaryTree<>();
			Lista<Servicio> auxiliar = new Lista<>();
			Servicio temporal = listaServicios.getIndex(i);
			if(temporal.getHarvesiana()!=0){
				if(tabla.get(temporal.getHarvesiana())!=null)
				{
					aux = new BinaryTree<>();
					auxiliar = new Lista<>();
					aux = tabla.get(temporal.getHarvesiana());
					if(aux.get(temporal.darTaxi().getTaxiId())!=null)
						auxiliar = aux.get(temporal.darTaxi().getTaxiId());
					auxiliar.add(temporal);
					aux.put(temporal.darTaxi().getTaxiId(), auxiliar);
					tabla.put(temporal.getHarvesiana(), aux);
				}
				else
				{
					aux = new BinaryTree<>();
					auxiliar = new Lista<>();
					auxiliar.add(temporal);
					aux.put(temporal.darTaxi().getTaxiId(), auxiliar);
					tabla.put(temporal.getHarvesiana(), aux);
				}
			}
		}
		
		if(tabla.get(millas)!=null)
			if(tabla.get(millas).get(taxiIDReq2C)!=null)
				resp = tabla.get(millas).get(taxiIDReq2C);
		return resp;
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		BinaryTree<Date, Lista<Servicio>> arbol = new BinaryTree<>();
		Lista<Servicio> auxiliar = new Lista<>();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss.000");
		Date dia = new Date();
		Lista<Servicio> resp = new Lista<>();
		try{
			dia = formato.parse(fecha+"-"+hora);
			int intervalo = (int) Math.round(dia.getMinutes()/15.);
			dia.setMinutes(intervalo*15);
		}
		catch(ParseException e){}

		for(int i = 0; i < listaServicios.size();i++)
		{
			Servicio temp = listaServicios.getIndex(i);
			Date aux = temp.darHorario().darFechaInicial();
			int intervalo = (int) Math.round(aux.getMinutes()/15.);
			aux.setMinutes(intervalo*15);
			if(arbol.contains(aux))
			{
				auxiliar = new Lista<>();
				auxiliar = arbol.get(aux);
				auxiliar.add(temp);
				arbol.put(aux, auxiliar);
			}
			else
			{
				auxiliar = new Lista<>();
				auxiliar.add(temp);
				arbol.put(aux, auxiliar);
			}
		}
		if(arbol.contains(dia))
		{
			auxiliar = new Lista<>();
			auxiliar = arbol.get(dia);
			for(int i = 0;i < auxiliar.size();i++)
			{
				Servicio temporal = auxiliar.getIndex(i);
				if(temporal.darZonaInicio()!=null && temporal.darZonaFinal()!=null)
					if(!temporal.darZonaInicio().equals(temporal.darZonaFinal()))
						resp.add(temporal);
			}

		}
		return resp;
	}

	private void heapify(TaxiConPuntos arreglo[], int contador, int i) {
		int max; 
		int nuevo;
		nuevo = 2 * i + 1;
		max = i;
		if (nuevo < contador)
			if (arreglo[nuevo].getPuntos() < arreglo[max].getPuntos())
				max = nuevo;
		if (nuevo + 1 < contador)
			if (arreglo[nuevo + 1].getPuntos() < arreglo[max].getPuntos())
				max = nuevo + 1;
		if (max != i) {
			TaxiConPuntos temp = arreglo[i];
			arreglo[i] = arreglo[max];
			arreglo[max] = temp;
			heapify(arreglo, contador, max);
		}
	}

	public void heap(TaxiConPuntos arreglo[]) {
		for (int i = arreglo.length / 2 - 1; i >= 0; i--)
			heapify(arreglo, arreglo.length, i);
		for (int i = arreglo.length - 1; i >= 1; i--) {
			TaxiConPuntos temp = arreglo[0];
			arreglo[0] = arreglo[i];
			arreglo[i] = temp;
			heapify(arreglo, i, 0);
		}
	}
	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{
	 // TODO Auto-generated method stub
	 final int R = 6371*1000; // Radious of the earth in meters
	 Double latDistance = Math.toRadians(lat2-lat1);
	 Double lonDistance = Math.toRadians(lon2-lon1);
	 Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))
	 * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
	 Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	 Double distance = R * c;
	 return distance;
	}

}
