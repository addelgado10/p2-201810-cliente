package model.data_structures;



public class Queue<E> implements IQueue<E> {
	
	private Node front;
	
	private Node back;
	
	private int tamanio;
		
	private class Node
	{
		E item;
		Node next;
	}
	
	public void enqueue(E item) {

		Node temp = back;
		back = new Node();
		back.item = item;
		back.next = null;
		if(isEmpty())
			front = back;
		else
			temp.next = back;
		tamanio++;
	}


	public E dequeue() {
		
		E item = front.item;
		front = front.next;		
		if(isEmpty())
			back = null;
		tamanio--;
		return item;
	}


	public boolean isEmpty() {

		return front == null;
	}


	public int size() {
		return tamanio;
	}
	
	public E first(){
		return front.item;
	}
}
