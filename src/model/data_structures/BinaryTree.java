package model.data_structures;

import com.sun.accessibility.internal.resources.accessibility;

public class BinaryTree<Key extends Comparable<Key>,Value> {

	private final static boolean RED= true;
	private final static boolean BLACK= false;
	private Lista<Key> llaves;
	
	
	private Node root;
	
	public class Node
	{
		private Key keys;
		
		private Value vals;
		
		private Node left;
		
		private Node rigth;

		private boolean colors;
		
		private int size;
		
		public Node(Key key, Value val, boolean color, int tamanio)
		{
			this.keys = key;
			
			this.vals = val;
			
			this.colors = color;
			
			this.size = tamanio;
		}
	}	
	public BinaryTree(){llaves = new Lista<>();}
	public void put(Key key, Value val)
	{
		root = put(root,key,val);
	}
	
	
	private Node put(Node actual, Key key, Value val) {
		
		if(actual ==null){
			llaves.add(key);
			return new Node(key,val, true,1);			
		}if(key.compareTo(actual.keys)<0)
			actual.left = put(actual.left,key, val);
		else if(key.compareTo(actual.keys)>0)
			actual.rigth = put(actual.rigth,key,val);
		else
			actual.vals = val;
		
		if(isRed(actual.rigth) && !isRed(actual.left))
			actual = rotarIzq(actual);

		
		if(isRed(actual.left)&&isRed(actual.left.left))
			actual = rotarDerecha(actual);

		
		if(isRed(actual.left)&&isRed(actual.rigth))
			cambiarColores(actual);

		
		actual.size = size(actual.left)+size(actual.rigth)+1;
		
		return actual;
	}

	private void cambiarColores(Node actual) {
		actual.colors = !actual.colors;
		actual.left.colors = !actual.left.colors;
		actual.rigth.colors = !actual.rigth.colors;
	}
	
	private Node rotarDerecha(Node actual) {
		Node x = actual.left;
		actual.left = x.rigth;
		x.rigth = actual;
		x.colors = actual.colors;
		actual.colors = RED;
		x.size = actual.size;
		actual.size = size(actual.left)+size(actual.rigth)+1;
		actual = x;
		return x;
	}
	
	private Node rotarIzq(Node actual) {
		Node x = actual.rigth;
		actual.rigth = x.left;
		x.left = actual;
		x.colors = actual.colors;
		actual.colors = RED;
		x.size = actual.size;
		actual.size = size(actual.left)+size(actual.rigth)+1;
		actual = x;
		return x;
		
	}
	private Node balancear(Node actual) {
		if(isRed(actual.rigth))
			actual = rotarDerecha(actual);

		
		if(isRed(actual.left)&&isRed(actual.left.left))
			actual = rotarIzq(actual);

		
		if(isRed(actual.left)&&isRed(actual.rigth))
			cambiarColores(actual);

		
		actual.size = size(actual.left)+size(actual.rigth)+1;
		
		return actual;
	}
	public Value get(Key key)
	{
		Node actual = root;
		
		while(actual!=null)
		{
			if(key.compareTo(actual.keys)<0)
				actual = actual.left;
			else if(key.compareTo(actual.keys)>0)
				actual = actual.rigth;
			else
				return actual.vals;
		}
		return null;
	}	
		
	public boolean isRed(Node actual)
	{
		if(actual==null)
			return false;
		return actual.colors;
	}
	
	public int size(Node actual)
	{
		if(actual == null)
			return 0;
		return actual.size;
	}
	
	public int size()
	{
		return size(root);
	}
	
	public boolean isEmpty()
	{
		return root==null;
	}
	
	public boolean contains(Key key)
	{
		return get(key)!=null;
	}
	
	public void delete(Key key)
	{
		if(!contains(key))
			return;
		if(!isRed(root.left)&&!isRed(root.rigth))
			root.colors = RED;

		root = delete(root,key);
		
		if(!isEmpty())
			root.colors = BLACK;
	}
	
	public Value getTop(){
		return root.vals;
	}
	private Node delete(Node node, Key key) {
		if(key.compareTo(node.keys)<0){
			if(!isRed(node.left)  && !isRed(node.left.left))
				node =moveRedLeft(node);
			node.left = delete(node.left,key);
		}
		else
		{
			if(isRed(node.left))
				node = rotarDerecha(node);
			if(key.compareTo(node.keys)==0 && node.rigth ==null)
				return null;
			if(!isRed(node.rigth)&&!isRed(node.rigth.left))
				node = moveRedRigth(node);
			if(key.compareTo(node.keys)==0)
			{
				Node min = sacarMin(node.rigth);	
				node.keys = min.keys;
				node.vals = min.vals;
				node.rigth = eliminarMin(node.rigth);
			}
			else
				node.rigth = delete(node.rigth,key);
		}
		return balancear(node);
	}
	private Node moveRedRigth(Node node) {
		cambiarColores(node);
		if(isRed(node.left.left))
		{
			node = rotarDerecha(node);
			cambiarColores(node);
		}
		return node;
	}

	private Node moveRedLeft(Node node) {
		cambiarColores(node);
		if(isRed(node.rigth.left))
		{
			node.rigth = rotarDerecha(node.rigth);
			node = rotarIzq(node);
			cambiarColores(node);
		}
		return node;
	}
	private Node sacarMin(Node node)
	{
		if(node.left==null)
			return node;
		else
			return sacarMin(node.left);
	}
	private Node eliminarMin(Node node)
	{
		if(node.left ==null)
			return null;
		else if(!isRed(node.left)&&!isRed(node.left.left))
			node = moveRedLeft(node);
		node.left= eliminarMin(node.left);
		return balancear(node );
	}
	public Lista<Key> getLLaves()
	{
		return llaves;
	}
	
}
