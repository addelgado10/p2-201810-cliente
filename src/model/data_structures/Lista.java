package model.data_structures;

import java.util.Iterator;

import model.vo.TaxiConServicios;

public class Lista<E> implements IList, Iterable{

	
	private class Node
	{
		public E item;
		public Node next;
	}
	
	private Node cabeza;
	
	
	private int tama�o;
	
	public Lista()
	{
		cabeza = new Node();
		cabeza.next = null;
		tama�o = 0;
	}
	
	public int size() {
		return tama�o;
	}

	 
	public boolean isEmpty() {
		return tama�o==0;
	}

	 
	public E first() {
		return cabeza.item;
	}

	public void addFirst(E element) {
		if (tama�o>0)
		{
			Node temp = new Node();
			temp = cabeza;
			cabeza = new Node();
			cabeza.item = (E) element;	
			cabeza.next = temp;
		}
		else
		{
			cabeza = new Node();
			cabeza.item = (E) element;
		}
		tama�o++;
	}
	 
	public void removeFirst() {
		if (!isEmpty())
		{
			cabeza = cabeza.next;
		}
		tama�o--;		
	}
	
	public void addIndex(int pos,E element)
	{
		if(isEmpty() || pos == 0)
		{
			addFirst(element);
		}
		else
		{
			Node actual = cabeza;
			for(int i = 0;i < pos-1;i++)
			{
				actual = actual.next;
			}
			
			Node temp = new Node();
			temp.next = actual.next;
			actual.next =temp;
			temp.item = (E) element;
			tama�o++;
		}
	}

	public void removeIndex(int pos)
	{
		if(pos == 0)
		{
			removeFirst();
		}
		else
		{
			int poss = pos+1;
			if(pos<=tama�o)
			{
				Node actual = new Node();
				Node anterior = new Node();
	
				actual = cabeza;
				for(int i = 0;i < poss-1;i++)
				{
					anterior = actual;
					actual = actual.next;
				}
				anterior.next = actual.next;
				tama�o--;
			}
		}

	}

	public void add(E element) {
		// TODO Auto-generated method stub
		if(tama�o==0)
		{
			cabeza= new Node();
			cabeza.item = (E) element;
			tama�o++;
		}
		else
		{
			Node actual = cabeza;
			while(actual.next!=null)
			{
				actual = actual.next;
			}
			
			Node temp = new Node();
			temp.next = null;
			actual.next = temp;
			temp.item = (E) element;
			tama�o++;
		}

	}
	 
	public E getIndex(int pos)
	{
		int poss = pos+1;
		if(pos<=tama�o)
		{
			Node actual = cabeza;
			for(int i = 0;i < poss-1;i++)
			{
				actual = actual.next;
			}
			return actual.item;
		}
		else
			return null;
	}
	public void set(int pos,E element)
	{
		int poss = pos+1;
		if(pos<=tama�o)
		{
			Node actual = cabeza;
			for(int i = 0;i < poss-1;i++)
			{
				actual = actual.next;
			}
			actual.item= element;
		}
	}

	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new IteratorObject();
	}

	public class IteratorObject implements Iterator
	{
		Node actual;
		
		public IteratorObject() {
			actual = cabeza;
		}
		
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual.next!=null;
		}

		@Override
		public E next() {
			E resp = actual.item;
			actual = actual.next;
			return resp;
		}
		
	}



}
