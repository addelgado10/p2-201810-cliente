package model.data_structures;

import model.data_structures.SeparateCahainingHash.Node;

public class SeparateCahainingHash <Key extends Comparable<Key>, Value> {

	private int size;
	private Node[] st;

	protected static class Node{

		private Object key;
		private Object element;
		private Node next;
		public Node(Object keys,Object vals, Node nextt)
		{
			this.key = keys;
			this.element =vals;
			this.next = nextt;
		}
	}

	public SeparateCahainingHash (int elements){

		int a = defineSize(elements);
		st = new Node[a];
	}


	/**
	 * Devuelve el elemento referenciado con su respectiva llave
	 * @param key: Llave para hallar elemento
	 * @return elemento relacionado con la llave
	 */
	public Value get(Key key){

		int a = hash(key);

		for(Node x = st[a];x!=null;x=x.next)
		{
			if(x.key.equals(key))
			{
				return  (Value) x.element;
			}
		}
		return null;	
	}

	/**
	 * Valuelimian un elemento del arreglo
	 * @param key
	 */
	public Value delete(Key key){
		if(get(key)==null)
			return null;
		
		int a = hash(key);
		Node temp = st[a];
		Node anterior = st[a];
		if(temp.key.equals(key))
			st[a] = st[a].next;
		else{
			for(Node x = st[a];x!=null;x=x.next)
			{
				if(x.key.equals(key)){
					anterior.next = anterior.next.next;
					return  (Value) x.element;
				}
				anterior = x;
			}
		}
		return (Value) temp.element;
		
	}

	/**
	 * Define la posicion del elemento en el arreglo
	 * Agrega a una linketlist las colisiones
	 * @param st: Valuelemento a agregar
	 */
	public void put(Key key,Value elem){
		int a = hash(key);
		for(Node x = st[a];x!=null;x=x.next)
		{
			if(x.key.equals(key)){
				x.element =  elem;
				return;
			}
		}
		st[a] = new Node(key,elem,st[a]);
	}
	
	public int size(){
		return size;
	}

	/**
	 * Definir el tama�o de la lista en un numero primo
	 * Garantiza factor de carga <=0.5
	 */
	private int defineSize(int elements){
		size= (int) Math.floor(elements*0.6);
		
		while(!isPrime(size))
		{
			size += 1;
		}
		return size;
	}

	/**
	 * define si un numero es primo o no
	 * @param prime: numero a revisar
	 * @return true si el numero es primo / false de lo contrario
	 */
	private boolean isPrime(int prime){

		int contador = 0;
        for(int i = 1; i <= prime; i++)
        {
            if((prime % i) == 0)
            {
                contador++;
            }
        }
 
        if(contador <= 2)
        {
            return true;
        }else{
            return false;
        }
		
	}


	private int hash(Key key){
		return (key.hashCode()& 0x7fffffff)%size;
	}



}
