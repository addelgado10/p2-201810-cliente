package model.data_structures;


public class Stack<E> implements IStack<E>{

	private Node first;
	
	private int tamanio;

	private class Node
	{
		E item;
		Node next;		
	}
	public void push(E obj) {	
		
		Node temp = first;
		first = new Node();
		first.item = obj;
		first.next = temp;
		tamanio++;
	}

	
	public E pop() {
		
		E resp = first.item;
		first = first.next;
		tamanio--;
		return resp;
	}

	
	public boolean isEmpty() {
		return first==null;
	}
	
	public int size()
	{
		return tamanio;
	}


}
