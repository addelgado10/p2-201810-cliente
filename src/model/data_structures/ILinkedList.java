package model.data_structures;

public interface ILinkedList<E> {
	
	public int size();
	
	public boolean isEmpty();
	
	public E first();
	
	public void addFirst(E element);
	
	public void removeFirst();
	
	public void addIndex(int pos, E element);
	
	public void removeIndex(int pos);
	
	public void addLast(E element);
	
	public E getIndex(int pos);
	
	public void set(int pos,E element);
	
}
